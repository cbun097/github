package com.example.claire.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void testGithub(){
        String message = " this is a test";
    }

    private void insertGithub(){
        String message = " this needs git addto be pushed to the github projet";
    }
    private void secondTest(){
        String message = " this is a second test to add to github";
    }
}
